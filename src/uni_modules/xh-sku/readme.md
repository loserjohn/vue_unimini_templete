# xh_sku 插件

> version: 1.1.0

## 描述

- 自用的 skulist 插件库，结构简单，ui 部分比价简陋，需要根据业务修改
- sku 选择 会根据 sku 库存 在使用过程中产生 禁用项

## 项目地址

## 参数配置

- porId 商品唯一值
- visible 双向绑定 sku 隐藏/显示
- couponList //优惠券参数选项 array

  ```
  	[
  		{
  			order_amount: 100,   //金额门槛
  			price: 20
  		},
  		{
  			order_amount: 200,   //优惠金额
  			price: 10
  		}
  	]
  ```

- styleOptions 基础的 底部按钮样式 及文字配置

  ```
  	{
  		cardBtnCorlor: '#d1a320',
  		buyBtnColor: '#50AB9F',
  		cardBtnText: '加入购物车吧！',
  		buyBtnText: '立即购买！',
  	}
  ```

- action 是单独显示 加入购物车 或者 立即 购买

  ```
  	 参数为  'buy','card','both'
  ```

- propertyList 配置除 sku 之外的其他额外 服务项；

  ```
  	 [{
  					service_type_name: '保修',
  					key: 'bx',   //唯一标识
  					required: false,   //是否必填
  					multiple: false,  // 是否多选
  					list: [{
  							checked: true,   //选填
  							service_name: '一年',
  							service_code: '342dfadsf',   //子项的唯一id
  							service_price: 35,
  						},
  						{
  							checked: true,
  							service_name: '两年',
  							service_code: '56gsdf',
  							service_price: 58
  						}
  					]
  				},
  				 ...
  			]
  ```

- parameter 所有的 规格像，展示顺序会直接体现在 UI 上

  ```
  	 [{
  					key: '123', //唯一标识
  					checked: true,  //选填
  					name: '颜色',
  					list: [{
  							checked: true,  //选填
  							name: '白色'
  						},
  						{
  							checked: true,
  							name: '黑色'
  						},
  						{
  							checked: true,
  							name: '绿色'
  						}
  					]
  				},
         ...
  			]
  ```

- difference 所有读取的排列组合数组

  ```
  	 [
  					{
  					id: '19',  //唯一标识
  					skus_price: 200,  //sku价格
  					skus_stock: 19,  //sku库存
  					skus_pic: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fup.enterdesk.com%2Fedpic%2Fec%2Fea%2F0f%2Fecea0f0a327ee84a8447ecddd997f663.jpg&refer=http%3A%2F%2Fup.enterdesk.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1653373440&t=bd9a3a86c79ce3addabdc36e91f34558',
  					difference: ['白色', 'LS', '豪华']   //规格标识
  				},
         ...
  			]
  ```

- defaultData 用户未选择任何 sku 时候的默认商品信息

  ```
  	 {
  					preImg: "https://img2.baidu.com/it/u=3895119537,2684520677&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
  					title: "测试标题",  //默认标题
  					price: 0,   //默认价格
  					stock: 0 //默认库存
  				}
  ```

  ## 插槽

- 目前只有一个默认插槽，位于额外服务项的 底部 用户业务上的扩展

  ## 注意项
-  切换商品时候需要 及时切换 proId 重新计算 sku
  
- difference 参数和 parameter 所列的数据属性顺序必须 对应例如：

  `parameter 为 颜色-->尺寸 --> 风格 那么生成的 difference 参数的顺序必须为 [ '红色','LS','简约 ']`

## 下版本扩展优化

- 1、考虑活动商品信息整合 和 展示

- 2、具名插槽更多配置项

- 3、 自定义主体样式 更灵活
