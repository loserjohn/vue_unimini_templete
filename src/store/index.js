/*
 * @Author: loserjohn
 * @Date: 2022-04-23 10:24:55
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 10:33:46
 * @Description: ---
 * @FilePath: \my-project\src\store\index.js
 */


import Vue from 'vue';
import Vuex from 'vuex';
// import http from '@/utils/http/index.js'
import app from './modules/app';
// import http from '@/utils/http/index.js'

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		loadApp: false
	},
	mutations: {
		SET_LOADAPP(state, bool) {
			console.log();
			state.loadApp = bool;
		}
	},

	getters: {
		gloadApp(state) {
			return `getter_${state.loadApp}`;
		},

		// eslint-disable-next-line no-unused-vars
		title({ state, commit }) {

			return `getter_app_${app.state.title}`;
		}
	},
	actions: {
		// eslint-disable-next-line no-unused-vars
		async loadConfig({ state, commit }, payload) {
			console.log(payload);
			setTimeout(() => {
				commit('SET_LOADAPP', true);
			}, 2500);
		}
		// async refreshUser({ state,commit }, payload){
		// 	let  that  = this
		// 	let ress =  await http.getConsumer();


		// 	if(ress.result===1){
		// 		commit('login')
		// 		commit('setAccountInfo',ress.data)
		// 	}else{
		// 		commit('logout')
		// 	}
		// }
	},
	modules: {
		namespaced: true,
		app
	}
});

export default store;
