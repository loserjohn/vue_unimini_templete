/*
 * @Author: loserjohn
 * @Date: 2022-04-23 10:26:31
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 10:27:11
 * @Description: --- 
 * @FilePath: \my-project\src\SET.js
 */

// 全局设置



export default {

	mainUrl: '',
	tokenName: 'rz_token', //储存的token名字
	wxInfoName: 'rz_wxInfo',  //各项目储存 用户信息的名字
	globalSetName: 'rz_globalSet', //各项目储存 字典项和用户配置
};