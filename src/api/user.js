/*
 * @Author: loserjohn
 * @Date: 2022-04-23 11:15:34
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 11:17:25
 * @Description: ---
 * @FilePath: \my-project\src\api\user.js
 */
import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function userInfo() {
  return request({
    url: '/userInfo',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/LoginOut',
    method: 'post'
  })
}
