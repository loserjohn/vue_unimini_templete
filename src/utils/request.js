/*
 * @Author: loserjohn
 * @Date: 2022-04-23 10:41:53
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 11:57:04
 * @Description: ---
 * @FilePath: \my-project\src\utils\request.js
 */
 
import ajax from 'uni-ajax'
let requstQueue = 0   //存放 同时请求的个数计数器  用来避免重复 loading
let taskQueue = []   //储存竞速队列 防止小程序login并发
let logining = false   //登录锁
let TOKEN = null  //全局token
const pendingRequest = new Map();    //记录相同请求的 队列 防止重复请求
// 创建请求实例
const instance = ajax.create({
	// 初始配置
	baseURL: process.env.VUE_APP_BASE_API,
	xhr: (requestTask, config) => { 
		addPendingRequest(config,requestTask)
	}
})


//小程序 获取  登录活去code
const login = async () => {
	return new Promise((rol, rej) => {
		uni.login({
			provider: "weixin",
			success: async ({
				code
			}) => {
				try {
					const {
						data
					} = await ajax.get({
						url: process.env.VUE_APP_BASE_API + '/login',
						data: {
							code
						}
					})
					rol(data.token)
				} catch (e) {
					rej(e)
					//TODO handle the exception
				}
			}
		})
	})

}
//获取  token 进行队列竞速  取消 login并发
const getToken = () => {
	return new Promise((rol, rej) => {

		if (logining) {
			// return false
			taskQueue.push(token => {
				rol(token)
			})
		} else {
			logining = true
			login().then(token => {
				if (taskQueue.length > 0) {
					taskQueue.map(it => {
						it(token)
					})
				}
				logining = false
				rol(token)
			}).catch(err=>{
				rej(err)
			})
		}

	})
}


//请求队列清除 
const clearPending=()=> {
  for (const [requestKey, cancelTask] of pendingRequest) {
    cancelTask.abort();
  }
  pendingRequest.clear()
}

//移除同名请求
const removePendingRequest=(config)=> {
  const requestKey = generateReqKey(config);
  console.log(0,pendingRequest.has(requestKey),requestKey)
  if (pendingRequest.has(requestKey)) {
    const cancelTask = pendingRequest.get(requestKey);
	
    cancelTask.abort();
	console.log('取消并发相同请求',pendingRequest,requestKey)
    pendingRequest.delete(requestKey);
  }
}

// 添加请求队列
const addPendingRequest=(config,task)=> {
  const requestKey = generateReqKey(config);
  if (!pendingRequest.has(requestKey)) {
	  console.log('添加队列',pendingRequest,requestKey)
     pendingRequest.set(requestKey, task);
  } 
}

// 生成请求唯一标识
const generateReqKey=(config)=> {
  let { method, url ,baseURL } = config; 
  const vurl =  url.replace(baseURL,'')
  return [method, vurl].join("_");
}
// 添加请求拦截器
instance.interceptors.request.use(
	async (config) => {	 
		removePendingRequest(config); // 检查是否存在重复请求，若存在则取消已发的请求 
		if (!TOKEN) {
			TOKEN = await getToken()
		} 
		config.header['Authorization'] = TOKEN
		return config
		},
		error => {
			// 对请求错误做些什么
			 pendingRequests.clear();
			return Promise.reject(error)
		}
)

// 添加响应拦截器
instance.interceptors.response.use(
	response => {
		// 对响应数据做些什么
		// debugger
		const res = response.data
		// console.log(res)
		hideLoading()
		if (response) {
			return res
		} else {
			errorHandle(res)

			return Promise.reject(res)
		}
		// if (res.status ===  401) {
		//   // to re-login

		// }else{

		// }
	},
	error => {
		// 对响应错误做些什么
		errorHandle(error)
		return Promise.reject(error)
	}
)

// 导出 create 创建后的实例
export default instance

function showLoading() { 
	if (requstQueue <= 0) {
		console.log('loading', requstQueue)
		uni.showLoading()
		requstQueue = 0
	}
	requstQueue += 1
}

function hideLoading() { 
	requstQueue -= 1
	if (requstQueue <= 0) {
		console.log('hideLoading', requstQueue)
		// loadingInstance.close();
		uni.hideLoading()
		// loadingInstance = null;
		requstQueue = 0
	}

}

function errorHandle(res) {
	console.log('errorHandle', res)
	// uni.showToast({
	// 	icon: 'none',
	// 	// title: 'error',
	// 	title: res.data.message,
	// })
}
