import md5 from 'js-md5'
import axios from 'axios'
// 分片上传
/*
* 分片上传函数 支持多个文件
* @param options
* options.file 表示源文件
* options.pieceSize 表示需要分片的大小 默认是5m
* options.chunkUrl 分片上传的后端地址
* options.fileUrl 整个文件的上传地址
* progress 进度回调
* success 成功回调
* error 失败回调
*/
export const uploadByPieces = ({ files, chunkUrl, fileUrl, pieceSize = 1, progress, success, error }) => {
  if (!files || !files.length) return
  // 上传过程中用到的变量
  const fileList = [] // 总文件列表
  let progressNum = 1 // 进度
  let successAllCount = 0 // 上传成功的片数
  // let currentAllChunk = 0 // 当前上传的片数索引
  let AllChunk = 0 // 所有文件的chunk数之和
  let AllFileSize = 0 // 所有文件size
  // 获取md5
  const readFileMD5 = (files) => {
    // 读取每个文件的md5
    files.map((file, index) => {
      const fileRederInstance = new FileReader()
      // fileRederInstance.readAsBinaryString(file)
      fileRederInstance.readAsArrayBuffer(file)
      console.log('文件buffer', fileRederInstance)
      fileRederInstance.addEventListener('load', e => {
        const fileBolb = e.target.result
        console.log(e)
        const fileMD5 = md5(fileBolb)
        console.log(fileMD5)
        if (!fileList.some((arr) => arr.md5 === fileMD5)) {
          fileList.push({ md5: fileMD5, name: file.name, file })
          AllFileSize = AllFileSize + file.size
        }
        if (index === files.length - 1) readChunkMD5(fileList)
      }, false)
    })
  }

  // slice切片
  const getChunkInfo = (file, currentChunk, chunkSize) => {
    const start = currentChunk * chunkSize
    const end = Math.min(file.size, start + chunkSize)
    const chunk = file.slice(start, end)
    return { start, end, chunk }
  }

  // 针对每个文件进行chunk处理
  const readChunkMD5 = (fileList) => {
    console.log('本次上传所有文件列表', fileList)
    fileList.map((currentFile, fileIndex) => {
      console.log('当前文件', currentFile)
      const chunkSize = pieceSize * 1024 * 1024 // 5MB一片
      const chunkCount = Math.ceil(currentFile.file.size / chunkSize) // 总片数
      console.log('总片数', chunkCount)
      AllChunk = AllChunk + chunkCount // 计算全局chunk数
      // let fileSize = currentFile.file.size // 文件大小
      // 针对单个文件进行chunk上传
      for (var i = 0; i < chunkCount; i++) {
        const { chunk } = getChunkInfo(currentFile.file, i, chunkSize)
        console.log('分片Blob' + (i + 1), chunk)
        const chunkFR = new FileReader()
        chunkFR.readAsArrayBuffer(chunk)
        chunkFR.addEventListener('load', e => {
          const chunkBolb = e.target.result
          console.log('变为array对象', chunkBolb)
          const chunkMD5 = md5(chunkBolb)

          // this.readingFile = false
          uploadChunk(currentFile, { chunkMD5, chunk, currentChunk: i, chunkCount }, fileIndex)
        }, false)
      }
    })
  }

  // 更新进度
  const progressFun = () => {
    progressNum = Math.ceil(successAllCount / AllChunk * 100)
    progress(progressNum)
  }

  // 对分片已经处理完毕的文件进行上传
  const uploadFile = (currentFile) => {
    const makeFileForm = new FormData()
    makeFileForm.append('md5', currentFile.fileMD5)
    makeFileForm.append('file_name', currentFile.name)
    console.log(makeFileForm)
    return
    axios({ // 合并文件
      method: 'post',
      url: fileUrl,
      data: makeFileForm
    }).then(res => {
      progressFun()
      res.file_name = currentFile.name
      success && success(res)
      successAllCount++
    }).catch(e => {
      error && error(e)
    })
  }

  // 分片上传
  const uploadChunk = (currentFile, chunkInfo, fileIndex) => {
    const fetchForm = new FormData()
    // fetchForm.append('file_name', currentFile.name)
    // fetchForm.append('md5', currentFile.md5)

    console.log('上传前', currentFile)
    console.log('上传前切片', chunkInfo.chunk)
    // fetchForm.append('chunks', chunkInfo.chunkCount)
    // fetchForm.append('chunk_index', chunkInfo.currentChunk)
    // fetchForm.append('chunk_md5', chunkInfo.chunkMD5)
    fetchForm.append('chunks', chunkInfo.chunkCount)
    fetchForm.append('chunk', chunkInfo.currentChunk)
    fetchForm.append('setting', 'up_cms_download')
    fetchForm.append('file', chunkInfo.chunk)
    // let n = new File([chunkInfo.chunk],'file')
    // console.log('上传的文件内容',n)
    // fetchForm.append('file',  n )
    // fetchForm.append('file', currentFile.file)
    // console.log(fetchForm)
    // console.log(chunkUrl)
    // let data={
    //   folder:chunkInfo.chunk,
    //   setting:'up_cms_download' ,
    //   chunks:0,
    //   chunk:0
    // }
    // console.log(data)
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
        Accept: '*/*'
      }
    }
    // alert(1)
    axios.post(chunkUrl, fetchForm, config).then(res => {
      progressFun()
      console.log(successAllCount)
      // currentAllChunk++
      if (chunkInfo.currentChunk < chunkInfo.chunkCount - 1) {
        successAllCount++
      } else {
        console.log(currentFile, fileIndex)
        // 当总数大于等于分片个数的时候
        if (chunkInfo.currentChunk >= chunkInfo.chunkCount - 1) {
          uploadFile(currentFile, fileIndex)
        }
      }
    }).catch((e) => {
      console.log('error')
      error && error(e)
    })
  }
  readFileMD5(files) // 开始执行代码
}
