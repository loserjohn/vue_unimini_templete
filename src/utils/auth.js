/*
 * @Author: loserjohn
 * @Date: 2021-11-30 21:30:46
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 11:08:50
 * @Description: ---
 * @FilePath: \my-project\utils\auth.js
 */

import { tokenName } from '@/SET'

export function getToken() {
  const c = uni.getStorageSync(tokenName)
  return c ? `Bearer ${c}` : null
}

export function setToken(token) {
  return uni.setStorageSync(tokenName, token)
}

export function removeToken() {
  return uni.removeStorageSync(tokenName)
}

// export function getMenus() {
//   const str = localStorage.getItem(MenusKey)
//   return str ? JSON.parse(str) : null
// }

// export function setMenus(m) {
//   if (!m) return
//   const str = JSON.stringify(m)
//   return localStorage.setItem(MenusKey, str)
// }

// export function removeMenus() {
//   return localStorage.removeItem(MenusKey)
// }



export function getLocal(m) {
  if (!m) return
  const res = uni.getStorageSync(m)
  return res || null
}
export function setLocal(m, v) {
  if (!m || !v) return
  const res = uni.setStorageSync(m, v)
  return res
}

export function removeLocal(m) {
  if (!m) return
  return uni.removeStorageSync(m)
}
