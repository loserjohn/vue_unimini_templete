export function arrayToTree(data, parent, idName, parentName) {
  var tree = []
  var temp
  for (var i = 0; i < data.length; i++) {
    if (data[i][parentName] === parent) {
      var obj = data[i]
      temp = arrayToTree(data, data[i][idName], idName, parentName)
      if (temp.length > 0) {
        obj.children = temp
      }
      tree.push(obj)
    }
  }
  return tree
}
