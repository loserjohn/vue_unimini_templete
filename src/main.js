/*
 * @Author: loserjohn
 * @Date: 2022-04-21 18:20:34
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 15:32:15
 * @Description: ---
 * @FilePath: \my-project\src\main.js
 */
import uView from 'uview-ui';
import Vue from 'vue';
import App from './App';
import store from './store';
Vue.use(uView)

Vue.config.productionTip = false;

App.mpType = 'app';
Vue.prototype.$store = store;
const app = new Vue({
  store,
  ...App
});
app.$mount();
