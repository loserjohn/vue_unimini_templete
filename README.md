# uniapp 开发小程序 cli 基础模板

## 1、集成内容

- 请求库使用的是 uni-ajax（utils 底下）,业务借口分割在 api 文件底下

- eslint + prettier 规范校验

- 开发环境隔离 （env 文件）

- sass 预处理

- uView 有其他需要可以替换 ui 库

- 基本结构
  - src `根目录`
    - api `业务请求`
    - pages `页面页面,需要考虑分包创建文件夹结构`
    - store `vuex模块`
    - utils `一些工具库 可以 使用lodash`
    - static `静态文件`
      - styles `css文件`

---

## 2、项目运行

- vuecli 创建 uniapp 项目

```
vue create -p dcloudio/uni-preset-vue my-project
```

- 安装包尽量使用 npm 或者 yarn

```
yarn 
```

- 默认运行 mp-weixin 端的 dev 环境测试包，域名配置在.env.development 里面

```
yarn serve
```

- 打包正式环境 mp-weixin 端生产包（代码压缩等），域名配置在.env.production 里面

```
yarn build
```

- 打包测试环境 mp-weixin 端生产包（代码压缩等），域名配置在.env.development 里面

```
yarn build:dev
```

---

## 3、参考要点

- 路由控制

  本项目暂时采用的是 uni 自身携带的路由机制，如果需要类似 vue-router 功能 做一些权限要求比较高的 前端项目也考虑使用 `uni-simple-router` 插件

  参考教程为：https://blog.csdn.net/weixin_42150719/article/details/111690178

  > 官方文档地址为：[uni-simple-router](https://hhyang.cn/v2/)

- 插件扩展

  本项目由使用 cli 生成，目的是实现一些开发扩展，和复用， 可以直接使用 vscode 开发，也可使用 hbuildx 开发。在引用其他插件的时候请使用 npm 安装方式。

- vscode 开发

  可以使用`uni-create-view` `uni-helper`等语法提示 ，或者直接在 huildx 里面开发提示更友善,可以直接使用右键创建 unipage 模板等。

- ui 库使用的是 uView

  主题色的 修改 是在 uni.scss 中 使用 scss 变量修改就好了。
  vscode 开发过程中可以使用 `uview-snippet` 插件 做语法提示。

  > ui2.x 官网：
  > [uView](https://www.uviewui.com/components/color.html)

- uni-ajx 开发
  近似与 axios 的使用方法。
  加强了 小程序处理并发login的问题，多页面同时调用权限接口，会排队竞速等待最快的login接口 返回用户信息。
  加强了短时间内重复请求的处理，短时间内只会发送同一个请求，其他会被abort取消
  加上了loding锁，通过计数器实现showloadig  和 hideloading  配对使用
  
  > uni-ajx 官网 [uni-ajx](https://uniajax.ponjs.com/guide/installation#npm)

- 常规配置可以写在 src/SET.js 底下

- 开发过程也可以直接使用 vue-ui 可视化界面 推荐

## 4、参考要点与方向

- 目前仍是使用 vuex 做状态管理，推荐使用 modules 进行业务分割 -后期稳定可以考虑使用 v3+pinia

- ci/cd 目前还没有接上 项目过程中会实现

- 很多地方没有完善，欢迎填坑


## 5、主要业务组件

- sku  商品sku组件demo