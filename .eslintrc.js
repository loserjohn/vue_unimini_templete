/*
 * @Author: loserjohn
 * @Date: 2021-11-30 21:30:45
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 15:47:54
 * @Description: ---
 * @FilePath: \my-project\.eslintrc.js
 */
module.exports = {
  root: true,

  parserOptions: {
    parser: '@babel/eslint-parser',
    sourceType: 'module'
  },

  env: {
    browser: true,
    node: true,
    es6: true,
  },

  extends: ['plugin:vue/recommended'],


  // eslint:recommended 


  // add your custom rules here
  //it is base on https://github.com/vuejs/eslint-config-vue
  rules: {
    'semi': [0, 'always'],
    'quotes': [2, 'single'],
    'no-console': 0,
    'no-empty': 2,
    'no-unused-vars': [2, {}],
    'vue/no-unused-components': [0, {
      'ignoreWhenBindingPresent': true
    }],
    'vue/no-parsing-error': [0, {}],
    'vue/no-v-html': [0, {}],
    'vue/require-prop-type-constructor': 0,
    'vue/max-attributes-per-line': [0, {
      'singleline': {
        'max': 4
      },
      'multiline': {
        'max': 1,
        allowFirstLine: false
      }
    }],
    'vue/singleline-html-element-content-newline': [0, {}]
  },

};
