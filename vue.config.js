/*
 * @Author: loserjohn
 * @Date: 2022-04-22 23:14:59
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-23 15:39:16
 * @Description: ---
 * @FilePath: \my-project\vue.config.js
 */
const path = require('path')

function resolve(dir) {
      return path.join(__dirname, dir)
}

module.exports = {
      transpileDependencies: ['@dcloudio/uni-ui', 'uni-ajax'],

      configureWebpack: {
            // provide the app's title in webpack's name field, so that
            // it can be accessed in index.html to inject the correct title.

            resolve: {
                  alias: {
                        '@': resolve('src')
                  }
            },
      }
}
